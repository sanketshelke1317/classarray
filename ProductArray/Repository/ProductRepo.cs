﻿using ProductArray.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductArray.Repository
{
    internal class ProductRepo
    {
        public Product[] products;

        public ProductRepo()
        {
            products = new Product[] { 
                new Product(1,"macbook",92000),
                new Product(2,"iphone",50200),
                new Product(3,"samsung",23000)
            };

        }
        public Product[] deleteItem(int id)
        {
            products = Array.FindAll(products, ele => ele.id != id);
            return products;
        }
        public Product[] updateProduct(int id,string name, int price)
        {
            int index = Array.FindIndex(products, ele => ele.id == id);
            products[index].price = price;
            products[index].name = name;
            return products;
        }
        public Product[] getAllProducts()
        {
            return products;
        }
    }
}
