﻿using ProductArray.Model;
using ProductArray.Repository;

ProductRepo productRepo = new ProductRepo();
//Product[] abc = productRepo.deleteItem(2);
//foreach(Product a in abc)
//{
//    Console.WriteLine($"id:{a.id} name:{a.name} price:{a.price}");
//}
Product[] upt = productRepo.updateProduct(1,"mobile",40030);
foreach(Product a in upt)
{
    Console.WriteLine($"id:{a.id} name:{a.name} price:{a.price}");
}
Product[] products = productRepo.getAllProducts();
Console.WriteLine("All products");
foreach(Product a in products)
{
    Console.WriteLine($"id:{a.id} name:{a.name} price:{a.price}");
}